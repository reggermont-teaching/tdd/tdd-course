// import fileReader from '../helpers/fileReader';

export default (string) => {
  if (typeof string !== 'string') {
    throw new TypeError(`Expected string, got ${typeof string}`);
  }

  return string.split(/[\s|\W]+/).filter(str => str !== '').length;
};

// export const fileWordCounter = (filePath) => {
//   /*
//    * - Use fileReader to open file and read content
//    * - Use sentenceWordCounter above
//    * - Return value
//    */
// };
